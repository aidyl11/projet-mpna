#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <time.h>

#define tol 0.00001


  // ----- Initialisation de la matrice ---//
  double* Init_Mat_toeplitz(double *A, int ligne, int colonne){

    //double *A;

   //A = malloc(ligne*colonne*sizeof(double));
    int k;
   for ( int i = 0; i < ligne; i++)
   {
      int k = 0;
     for ( int j = i; j < colonne; j++)
     {
        //A[i*ligne+j] = rand() *1.0 / RAND_MAX /*% 10*/;
        A[  i * colonne + j ] =  k++;
     }
     k = i;
    for ( int j = 0; j < i+1; j++)
     {
        //A[i*ligne+j] = rand() *1.0 / RAND_MAX /*% 10*/;
        A[(i)*ligne+(j)] = k--;
     }
     
    }

  return A;

}  

  double* Init_Mat(double *A, int ligne, int colonne){

    //double *A;

   //A = malloc(ligne*colonne*sizeof(double));
    int k;
   for ( int i = 0; i < ligne; i++)
   {
      int k = 0;
     for ( int j = 0; j < colonne; j++)
     {
        A[i*ligne+j] = rand() *1.0 / RAND_MAX /*% 10*/;
             
      }

    }

  return A;

}  


  // ----- affichage de la matrice ---//

   void Affiche_Mat(double* A, int ligne, int colonne){ 
  
   for ( int i = 0; i < ligne; i++)
     {
      for ( int j = 0; j< colonne; j++)
      {
        A[i*ligne+j];
        printf(" %f",A[i*colonne+j]);
      }

      printf("\n");
     }
   
   }

 // ----- Initilisation du vecteur ---//

 double* Init_Vecteur(double* V, int N)
{
   
   V=(double*)malloc(N*sizeof(double));

   for ( int i = 0; i <N; i++)
  {
    V[i] = rand() * 1. / RAND_MAX/*% 10*/; 
  }

  return V;
 }
 
 // ----- Affichage du vecteur ---//
 void Affich_Vecteur(double* V, int N){
   
   

    for (int i = 0; i <N; i++)
    {
     V[i];
       printf("%f\n",V[i] );
    }
  
    printf("\n");
   }

   // ----- Produit scalaire entre deux vecteurs ---//

    double Produit_Scalaire(double* V1, double* V2, int SV1, int SV2){
    if(SV1==SV2){
    double PS;
     for(int i=0;i<SV1;i++){  //ou SV2
         PS +=(double)V1[i]*V2[i];
       }
      return PS;
  }
     else{
      printf("erreur la taille des deux vexteurs sont différente\n");
     }


    }

    // ----- Produit de deux matrices x une colonne ---//
   
     double* Produit_Mat_Mat(double* M, double* N, int rowM,int rowN){

       double*  PM; //notre matrice resultat
         PM= malloc(rowM*rowN*sizeof(double));
       if(rowM==rowN){
       
        for(int i=0; i<rowM; i++){
   
           PM[i]=0;
     
         for(int j=0; j<rowN; j++){
            PM[i] +=  M[i*rowM+j]*N[j*rowN+j];
       
           }
         }


         }
         else{
              printf("la taille des deux matrice différente\n");
            }
      return PM;
     
    }
// ----- Produit entre un vecteur et un scalaire ---//
  double* Produit_Vect_Scalaire(double* V, int SV,double alpha){
      double* PSV;
      PSV =(double*) malloc(SV*sizeof(double));
  
    for(int i=0; i<SV; i++){
        PSV[i] += alpha*V[i];
     }
     return PSV;
   }

 // ----- Produit matrice vecteur ---//

  double* Produit_Mat_Vect(double *M,double *V, int SV){
       
       double* PMV=(double*)malloc(SV*sizeof(double));
        
        for(int i=0; i<SV; i++){

          PMV[i]=0.0;
           for(int j=0; j<SV;j++){
            PMV[i] += M[i*SV+j]*V[j];

           }
        }
        



        return PMV;

 }

  // ----- Produit d'une matrice fois une colonne d'un vecteur return(vecteur)---//
  double* Produit_MVect_col(double* M, double* Vect, int n, int m, int col){
      double* P=(double*)malloc(n*sizeof(double));
      for(int i=0; i<n; i++){
       P[i]=0.0;
       for(int j=0;j<n;j++){

          P[i] +=  M[i*n+j]*Vect[j*n + col];

         }
       

       }
    return P;    
  }
 //
  double* Prod_MVect_nm(double *M,double *V, int n, int m) {
 
   double *P=(double*)malloc(n*sizeof(double));
    
    for(int i=0; i<n; i++)
     {
     P[i]=0.0;
     for(int j=0; j<m; j++)
     {
      P[i]=P[i]+M[i*m+j]*V[j];
    }
  }
    
  return P;
}




 // ----- Produit d'un vecteur fois une colonne d'une matice return(PV scalaire)---//
  double Produit_Vect_ColVect(double* Vect1, double* Vect2, int n, int m, int col){
   double P=0.0;

   for(int i=0; i<n ; i++){
     P += Vect1[i]*Vect2[ i*m + col];
   }
   return P;
  }


 // ----- Calcul la norme d'un Vecteur ---//
 double Norme_vecteur(double* vect1,int n){
  double norme =0.0;
    for(int i=0; i<n ;i++){
        norme  += vect1[i]*vect1[i];

       }
      

       return sqrt(norme);

    }

 // 
double* Sub_Col_fVect(double* Vect, double f, double *Vec, int n, int m, int col) {
 
 // printf("w = %lf, %lf, %lf \n",f, w[0], V[n + col]); 
  for(int i=0; i<n; i++)
  {
    Vect[i] =  Vect[i] - f*Vec[i*m+col];
  
  }
  return Vect;
}
// ----- Addition entre deux vecteurs  ---// 
double* Add_Vect_Vect(double *Vec1, double* Vec2, int n){

  double* res= malloc(n*sizeof(double));
  for (int i=0;i<n;i++){
    res[i] = Vec1[i] + Vec2[i];
  }
  return res;

} 

// ----- Soustraction entre deux vecteurs  ---// 

double* Sub_Vect_Vect(double *Vec1, double* Vec2, int n){

  double* res= malloc(n*sizeof(double));
  for (int i=0;i<n;i++){
    res[i] = Vec1[i] - Vec2[i];
  }
  return res;

} 

/*double* transpose(double*A,int col,int row) {
	double *B=calloc(col*row,sizeof(double));
	for (int i = 0; i < col; i++) {
		for (int j = 0; j < row; j++)
      			B[i*row+j] = A[j*col+i];
	}
	return B;
}*/

/*double VERIF(double* Vm, int col,int row) {

	double* Vm_t = malloc(col*row*sizeof(double));
	double *I=calloc(col*row, sizeof(double));
	
	int res = 1;
	Vm_t = transpose(Vm, col, row);
	Produit_Mat_Mat(Vm,Vm_t,col,row);
  }*/

  void arnoldi (double *A, double *b, int n, double **hm, double **vm, int m){

  double *Vm = calloc(n*(m+1), sizeof(double));
  double *Hm = calloc((m+1)*m, sizeof(double));
  *vm = Vm;
  *hm = Hm;

  double *q = calloc(n, sizeof(double));

int i;
  //read_matrix(Vm,1,n);
  double norm_b = Norme_vecteur(b, n);
  for (int k = 0; k < n; k++) {
      double res = b[k]/norm_b;
      //printf("res = %f\n",res);
      Vm[k*(m+1)] = res;
      q[k] = res;      
    }

  //Affiche_Mat(Vm, n, m+1);

  for (int k = 0; k < m; k++) {
    double *Aq = Produit_Mat_Vect(A, q, n);
    // read_matrix(c,1,n);
    for (int j = 0; j <= k; j++) {
        double hij = Produit_Vect_ColVect(Aq, Vm, n, m+1, j);
        //printf("hij = %f \n", hij);
        Hm[ j*m + k] = hij;
        for (i=0; i < n; i++) Aq[i] = Aq[i] - hij*Vm[ i*(m+1) + j];
    }

    Hm[ (k+1)*m + k] = Norme_vecteur(Aq, n);

    double eps = 1e-12;

    //printf("Hm:\n");
    //Affiche_Mat(Hm, m+1, m);


    if (Hm[ (k+1)*m + k] > eps){
        
        for (i=0; i < n; i++){
          q[i] = Aq[i] / Hm[ (k+1)*m + k];
          Vm[ i*(m+1) + k+1 ] = q[i];
        }
    }
    else{
      return;
    } 
  }
    
}



int main(int argc , char* argv){

double t0 = 0., t1 = 0., duration = 0., temps;
clock_t debut,fin;
debut = clock();
//int col, row;

  int n = 8;
  int k, i;
  int m = 5;

double* b,*x0,*x,*r0,*v1,*wj,*ym,*e1,*xx,*res, *Vt;
double* Ax0;
double Beta,hij,h_new,normb,err,erreur;

  double *A = calloc(n*n, sizeof(double));
  
  double *Vm;
  // = malloc(n*(m+1)*sizeof(double));
  double *Hm;
  // = calloc((m+1)*m, sizeof(double));
   //double *I;
  

e1 = malloc(n*sizeof(double));
r0 = malloc(n*sizeof(double));
v1 = malloc(n*sizeof(double));
ym = malloc(n*sizeof(double));
x0 = malloc(n*sizeof(double));
b = malloc(n*sizeof(double));




double norm_b = Norme_vecteur(b,n);

for(int i=0; i<n; i++){ b[i] = rand()*1.0/RAND_MAX;}

for(int i=0; i<n; i++){ x0[i] =  b[i];}

//for(int i=0; i<n*n; i++){ A[i] =  i;}


printf("vecteur b :\n");
Affich_Vecteur(b,n);

Init_Mat_toeplitz(A, n, n);
printf("matrice A:\n");
Affiche_Mat(A, n, n);
//Init_Mat(A, n, n);

//printf("AA:\n");
//Affiche_Mat(A, n, n);


//m = 5;
arnoldi(A, b, n, &Hm, &Vm, m);

printf("Hm:\n");
Affiche_Mat(Hm, m+1, m);

printf("Vm:\n");
Affiche_Mat(Vm, n, m+1);  

int iter = 0;
int mm = 5;
norm_b = Norme_vecteur(b, n);

  //printf("x0:\n");
  //Affich_Vecteur(x0, n);
  double *Ax =  Produit_Mat_Vect(A, x0, n);

  //printf("Ax:\n");
  //Affich_Vecteur(Ax, n);

  for(int i=0; i<n; i++){ r0[i] =  b[i] - Ax[i];}
  
  arnoldi(A, r0, n, &Hm, &Vm, mm); 

   //*I = VERIF(Vm,n ,m+1);
 
//printf("I:\n");
//Affiche_Mat(I, col, row);  

fin = clock();
temps = ((double)fin - debut) /CLOCKS_PER_SEC;
fprintf(stdout, "Le temps d'execution : %lf s\n",temps);


  //free
  free(A); 

  free(b); 
  free(x0); 
  free(r0); 
  free(Hm); 
  free(Vm); 


  return 0;
}
