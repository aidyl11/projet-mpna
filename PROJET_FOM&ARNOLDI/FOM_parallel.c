#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <mpi.h>
//#define size 
#define tol 0.00001


//la factorisation LU

double* cutLUmatrix(double *L,double *U,double *A ,int n){
int i=0,j=0;
    for(i=0;i<n;i++)                      
        {         
         for(j=0;j<i+1;j++) L[n*i+j]=A[n*i+j];
         for(j=i;j<n;j++) U[n*i+j]=A[n*i+j];  
         L[i*n+i]=1;   
        }

 return A;
    
}

double* LUdecomp(double* A,int n){
int i=0,j=0,k=0;
 for(i=0;i<n-1;i++)
    {
    for(j=i+1;j<n;j++)
     {
     A[n*j+i]=A[n*j+i]/A[n*i+i];
     for(k=i+1;k<n;k++) 
        A[n*j+k]=A[n*j+k]-A[n*j+i]*A[n*i+k];
     }
    }
    return A;
}


double* solveTriangulaire(double *x,double *LU,double *b,int n) { //pas d alloc pour y;Axi=ei
int i=0,j=0;
    x[0]=b[0];
    for(i=0;i<n;i++)
    {
  for(j=0;j<i;j++) 
    {
    x[i]= b[i]- x[j]*LU[n*i+j];
      b[i]=x[i];
    }
    }
    
     x[n-1]=b[n-1]/LU[n*(n-1)+n-1];

     for(i=n-2;i>=0;i--)
  {
  for(j=n-1;j>i;j--) 
     { 
     x[i]= b[i]-x[j]*LU[n*i+j];
     b[i]=x[i]; 
     }
        x[i]/=LU[n*i+i];
       }

   return x;

}

double* solvesystem(double *x,double *A,double *b,int n){//resolution de Ax=b en ecrasant A et b.
LUdecomp(A,n);
x=solveTriangulaire(x,A,b,n);
return x;
}

  // ----- Initialisation de la matrice ---//
  double* Init_Mat_toeplitz(double *A, int ligne, int colonne){

    //double *A;

   //A = malloc(ligne*colonne*sizeof(double));
    int k;
   for ( int i = 0; i < ligne; i++)
   {
      int k = 0;
     for ( int j = i; j < colonne; j++)
     {
        //A[i*ligne+j] = rand() *1.0 / RAND_MAX /*% 10*/;
        A[  i * colonne + j ] =  k++;
     }
     k = i;
    for ( int j = 0; j < i+1; j++)
     {
        //A[i*ligne+j] = rand() *1.0 / RAND_MAX /*% 10*/;
        A[(i)*ligne+(j)] = k--;
     }
     
    }

  return A;

}  

  double* Init_Mat(double *A, int ligne, int colonne){

    //double *A;

   //A = malloc(ligne*colonne*sizeof(double));
    int k;
   for ( int i = 0; i < ligne; i++)
   {
      int k = 0;
     for ( int j = 0; j < colonne; j++)
     {
        A[i*ligne+j] = rand() *1.0 / RAND_MAX /*% 10*/;
             
      }

    }

  return A;

}  


  // ----- affichage de la matrice ---//

   void Affiche_Mat(double* A, int ligne, int colonne){ 
  
   for ( int i = 0; i < ligne; i++)
     {
      for ( int j = 0; j< colonne; j++)
      {
        A[i*ligne+j];
        printf(" %f",A[i*colonne+j]);
      }

      printf("\n");
     }
   
   }

 // ----- Initilisation du vecteur ---//

 double* Init_Vecteur(double* V, int N)
{
   
   V=(double*)malloc(N*sizeof(double));

   for ( int i = 0; i <N; i++)
	{
		V[i] = rand() * 1. / RAND_MAX/*% 10*/; 
	}

  return V;
 }
 
 // ----- Affichage du vecteur ---//
 void Affich_Vecteur(double* V, int N){
   
   

    for (int i = 0; i <N; i++)
	  {
		 V[i];
		   printf("%f\n",V[i] );
	  }
  
    printf("\n");
   }

   // ----- Produit scalaire entre deux vecteurs ---//

    double Produit_Scalaire(double* V1, double* V2, int SV1, int SV2){
    if(SV1==SV2){
    double PS;
    #pragma omp parallel for reduction(+:prod)

     for(int i=0;i<SV1;i++){  //ou SV2
  	  	 PS +=(double)V1[i]*V2[i];
       }
    	return PS;
 	}
     else{
      printf("erreur la taille des deux vexteurs sont différente\n");
     }


    }

    // ----- Produit de deux matrices x une colonne ---//
   
     double* Produit_Mat_Mat(double* M, double* N, int rowM,int rowN){

       double*  PM; //notre matrice resultat
         PM= malloc(rowM*rowN*sizeof(double));

         #pragma omp parallel for

       if(rowM==rowN){
       
        for(int i=0; i<rowM; i++){
   
           PM[i]=0;
     
         for(int j=0; j<rowN; j++){
            PM[i] +=  M[i*rowM+j]*N[j*rowN+j];
       
           }
         }


         }
         else{
              printf("la taille des deux matrice différente\n");
            }
      return PM;
     
    }
// ----- Produit entre un vecteur et un scalaire ---//
  double* Produit_Vect_Scalaire(double* V, int SV,double alpha){
      double* PSV;
      PSV =(double*) malloc(SV*sizeof(double));
  #pragma omp parallel for
    for(int i=0; i<SV; i++){
        PSV[i] += alpha*V[i];
     }
     return PSV;
   }

 // ----- Produit matrice vecteur ---//

  double* Produit_Mat_Vect(double *M,double *V, int SV){
       
       double* PMV=(double*)malloc(SV*sizeof(double));

        
        for(int i=0; i<SV; i++){

          PMV[i]=0.0;
           for(int j=0; j<SV;j++){
            PMV[i] += M[i*SV+j]*V[j];

           }
        }
        



        return PMV;

 }
/*********** Fonction qui fait le produit matrice vecteur *********************/
   double * prod_mat_vect_paral(double * M, double * V, int nrow, int ncol, int rank, int size)
     {
       double * local_mat;
       int debut_i;
       int fin_i ;
       int count;
       double * y = malloc(nrow * sizeof(double));

       //  allocation des buffers pour le calcul locale + répartition des taches entre processus
       if (rank < (nrow % size))
       {
       local_mat = malloc(sizeof(double) * (nrow / size) + 1);
       debut_i =  (rank * (nrow / size) + rank );
       fin_i = debut_i +(nrow / size) + 1 ;
       count = ((nrow / size) + 1);
        }
       else
       {
       local_mat = malloc(sizeof(double) * (nrow / size));
       debut_i = (rank * (nrow / size) + (nrow % size));
       fin_i = debut_i + (nrow / size);
       count = (nrow / size);
       }
       //double * local_mat = malloc(count * sizeof(double));
       for(int i = 0; i < fin_i - debut_i; i++)
       {
         double tmp = 0.0;
         for(int j = 0; j < nrow; j++)
         {
          //PMV[i] += M[i*SV+j]*V[j];

         tmp += M[debut_i + i*size+j]* V[j];
         }
         local_mat[i] = tmp;
       }
       MPI_Allgather(local_mat, count, MPI_DOUBLE, y, count, MPI_DOUBLE, MPI_COMM_WORLD);

         return y;

  }







  // ----- Produit d'une matrice fois une colonne d'un vecteur return(vecteur)---//
  double* Produit_MVect_col(double* M, double* Vect, int n, int m, int col){
      double* P=(double*)malloc(n*sizeof(double));
      #pragma omp parallel for
      for(int i=0; i<n; i++){
       P[i]=0.0;
       for(int j=0;j<n;j++){

          P[i] +=  M[i*n+j]*Vect[j*n + col];

         }
       

       }
    return P;    
  }
 //
  double* Prod_MVect_nm(double *M,double *V, int n, int m) {
 
   double *P=(double*)malloc(n*sizeof(double));
   #pragma omp parallel for
    
    for(int i=0; i<n; i++)
     {
     P[i]=0.0;
     for(int j=0; j<m; j++)
     {
      P[i]=P[i]+M[i*m+j]*V[j];
    }
  }
    
  return P;
}




 // ----- Produit d'un vecteur fois une colonne d'une matice return(PV scalaire)---//
  double Produit_Vect_ColVect(double* Vect1, double* Vect2, int n, int m, int col){
   double P=0.0;
    #pragma omp parallel for

   for(int i=0; i<n ; i++){
     P += Vect1[i]*Vect2[ i*m + col];
   }
   return P;
  }


 // ----- Calcul la norme d'un Vecteur ---//
 double Norme_vecteur(double* vect1,int n){
  double norme =0.0;
    for(int i=0; i<n ;i++){
        norme  += vect1[i]*vect1[i];

       }
      

       return sqrt(norme);

    }

 // 
double* Sub_Col_fVect(double* Vect, double f, double *Vec, int n, int m, int col) {
 
 // printf("w = %lf, %lf, %lf \n",f, w[0], V[n + col]); 
  #pragma omp parallel for

  for(int i=0; i<n; i++)
  {
    Vect[i] =  Vect[i] - f*Vec[i*m+col];
  
  }
  return Vect;
}
// ----- Addition entre deux vecteurs  ---// 
double* Add_Vect_Vect(double *Vec1, double* Vec2, int n){

  double* res= malloc(n*sizeof(double));
  #pragma omp parallel for
  for (int i=0;i<n;i++){
    res[i] = Vec1[i] + Vec2[i];
  }
  return res;

} 

// ----- Soustraction entre deux vecteurs  ---// 

double* Sub_Vect_Vect(double *Vec1, double* Vec2, int n){

  double* res= malloc(n*sizeof(double));
  #pragma omp parallel for
  for (int i=0;i<n;i++){
    res[i] = Vec1[i] - Vec2[i];
  }
  return res;

} 





void arnoldi (double *A, double *b, int n, double **hm, double **vm, int m, int rank,int size){

  double *Vm = calloc(n*(m+1), sizeof(double));
  double *Hm = calloc((m+1)*m, sizeof(double));
  *vm = Vm;
  *hm = Hm;

  double *q = calloc(n, sizeof(double));

int i;
  //read_matrix(Vm,1,n);
  double norm_b = Norme_vecteur(b, n);
  for (int k = 0; k < n; k++) {
      double res = b[k]/norm_b;
      //printf("res = %f\n",res);
      Vm[k*(m+1)] = res;
      q[k] = res;      
    }

  //Affiche_Mat(Vm, n, m+1);

  for (int k = 0; k < m; k++) {
    double *Aq = prod_mat_vect_paral(A, q, n,n,rank,size);
    //double * prod_mat_vect_paral(double * M, double * V, int nrow, int ncol, int size_v, int rank, int size)
    // read_matrix(c,1,n);
    for (int j = 0; j <= k; j++) {
        double hij = Produit_Vect_ColVect(Aq, Vm, n, m+1, j);
        //printf("hij = %f \n", hij);
        Hm[ j*m + k] = hij;
        for (i=0; i < n; i++) Aq[i] = Aq[i] - hij*Vm[ i*(m+1) + j];
    }

    Hm[ (k+1)*m + k] = Norme_vecteur(Aq, n);

    double eps = 1e-12;

    //printf("Hm:\n");
    //Affiche_Mat(Hm, m+1, m);


    if (Hm[ (k+1)*m + k] > eps){
        
        for (i=0; i < n; i++){
          q[i] = Aq[i] / Hm[ (k+1)*m + k];
          Vm[ i*(m+1) + k+1 ] = q[i];
        }
    }
    else{
      return;
    } 
  }
     //Sum = calloc(n, sizeof(double));
    //double_memset(Sum, 0., n);
 // read_matrix(vect2,1,n);
   //read_matrix(&Vm[j*n],1,n);
}



int main(int argc , char* argv){


  MPI_Init(&argc, &argv);
      int size; MPI_Comm_size(MPI_COMM_WORLD, &size);
      int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);

double t0 = 0., t1 = 0., duration = 0., temps;
clock_t debut,fin;
debut = clock();

  int n = 400;

  int k, i;
  int m = 40;

double* b,*x0,*x,*r0,*v1,*wj,*ym,*e1,*xx,*res, *Vt;
double* Ax0;
double Beta,hij,h_new,normb,err,erreur;
double *A = calloc(n*n, sizeof(double));
double *Vm;
double *Hm;

e1 = malloc(n*sizeof(double));
r0 = malloc(n*sizeof(double));
v1 = malloc(n*sizeof(double));
ym = malloc(n*sizeof(double));
x0 = malloc(n*sizeof(double));
b = malloc(n*sizeof(double));




double norm_b = Norme_vecteur(b,n);

  for(int i=0; i<n; i++){ b[i] = rand()*1.0/RAND_MAX;}

  for(int i=0; i<n; i++){ x0[i] =  b[i];}

//printf("vecteur b :\n");
//Affich_Vecteur(b,n);

Init_Mat_toeplitz(A, n, n);
//printf("matrice A:\n");
//Affiche_Mat(A, n, n);

int iter = 0;
int mm = 10;
norm_b = Norme_vecteur(b, n);

  //printf("x0:\n");
  //Affich_Vecteur(x0, n);


double TOLERANCE = 1e-6;
  while(iter++ < 600){

    if (mm > n) mm = n;
    e1 = calloc(mm, sizeof(double));
    double *Ax =  prod_mat_vect_paral(A, x0, n, n, rank, size);
    
    //printf("Ax:\n");
    //Affich_Vecteur(Ax, n);

    for(int i=0; i<n; i++){ r0[i] =  b[i] - Ax[i];}
    
  arnoldi(A, r0, n, &Hm, &Vm, mm, rank, size);


  Beta = Norme_vecteur(r0, n);
  //printf("Beta:%f\n",Beta);
  e1[0] = Beta;
  //printf("e1:\n");
  //Affich_Vecteur(e1, mm);

  solvesystem(ym, Hm, e1, mm);
  //printf("ym:\n");
  //Affich_Vecteur(ym, mm);

  //printf("Vm:\n");
  //Affiche_Mat(Vm, n, mm+1);
  //double *res = Produit_Mat_Vect(Vm, ym, mm);
  double *res = prod_mat_vect_paral(A, x0, n, n, rank, size);
  //printf("res:\n"); 
  //Affich_Vecteur(res, n);

  for(int i=0; i<n; i++){ x0[i] =  x0[i] + res[i];}
  res = prod_mat_vect_paral(A, x0, n, n, rank,size);
//double * prod_mat_vect_paral(double * M, double * V, int nrow, int ncol, int size_v, int rank, int size)

  for(int i=0; i<n; i++){ res[i] =  res[i] - b[i];}

  double err = Norme_vecteur(res, n) ;1/ norm_b;

  if (err < TOLERANCE){

    printf("break[%d]: err = %0.12lf \n", iter, err);
    break;
  } 
 
  //printf("err: %0.17lf \n", err);
}

//printf("vecteur x(solution du systeme lineaire) :\n");
//Affich_Vecteur(x0, n);

fin = clock();
temps = ((double)fin - debut) /CLOCKS_PER_SEC;
fprintf(stdout, "Le temps d'execution : %lf s\n",temps);



  FILE *frx = NULL ;
  frx=fopen("Vecteur_resultat.txt","w");  
  
    if(frx!=NULL){ 
  
  fprintf (frx,"resultat systéme linéaire:\n\n[");
  for (i=0; i<n; i++){
  fprintf(frx,"%f ", x0[i]);}
  if(i!=(n-1)){ fprintf(frx,", ]");} 
  }
  else {

      fprintf(stderr,"[Erreur] Impossible d'ouvrir le fichier\n");
    exit(1); 
  
  }


  //free
  free(A); 

  free(b); 
  free(x0); 
  free(r0); 
  free(Hm); 
  free(Vm); 
MPI_Finalize();

  return 0;
}

